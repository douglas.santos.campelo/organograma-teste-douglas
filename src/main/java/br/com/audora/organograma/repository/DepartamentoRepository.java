package br.com.audora.organograma.repositories;

import br.com.audora.organograma.models.Departamento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DepartamentoRepository extends JpaRepository<Departamento, Long> {

    // Exemplos de métodos de consulta personalizados

    List<Departamento> findByNome(String nome);

    List<Departamento> findByUnidadeId(Long unidadeId);

    @Query("SELECT d FROM Departamento d WHERE d.nome LIKE %:keyword%")
    List<Departamento> searchByKeyword(@Param("keyword") String keyword);


}
