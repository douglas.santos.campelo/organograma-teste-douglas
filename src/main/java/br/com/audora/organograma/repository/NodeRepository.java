package br.com.audora.organograma.repositories;

import br.com.audora.organograma.models.Node;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NodeRepository extends JpaRepository<Node, Long> {

    // Exemplos de métodos de consulta personalizados

    List<Node> findByVinculo_Pessoa_Nome(String nome);

    List<Node> findByOrganogramaIdAndVinculo_DepartamentoId(Long organogramaId, Long departamentoId);

    @Query("SELECT n FROM Node n WHERE n.vinculo.pessoa.nome LIKE %:keyword%")
    List<Node> searchByKeyword(@Param("keyword") String keyword);



}