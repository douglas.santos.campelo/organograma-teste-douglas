package br.com.audora.organograma.repositories;

import br.com.audora.organograma.models.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

    Optional<Pessoa> findByNome(String nome);

    List<Pessoa> findByUnidadeId(Long unidadeId);

    @Query("SELECT p FROM Pessoa p WHERE p.nome LIKE %:keyword%")
    List<Pessoa> searchByKeyword(@Param("keyword") String keyword);


}
