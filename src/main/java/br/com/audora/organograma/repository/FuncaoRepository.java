package br.com.audora.organograma.repositories;

import br.com.audora.organograma.models.Funcao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FuncaoRepository extends JpaRepository<Funcao, Long> {

    // Exemplos de métodos de consulta personalizados

    List<Funcao> findByNome(String nome);

    List<Funcao> findByDepartamentoId(Long departamentoId);

    @Query("SELECT f FROM Funcao f WHERE f.nome LIKE %:keyword%")
    List<Funcao> searchByKeyword(@Param("keyword") String keyword);


}
