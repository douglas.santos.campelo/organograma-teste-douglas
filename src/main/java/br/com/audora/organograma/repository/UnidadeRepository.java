package br.com.audora.organograma.repositories;

import br.com.audora.organograma.models.Unidade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UnidadeRepository extends JpaRepository<Unidade, Long> {

    Optional<Unidade> findByNome(String nome);

    List<Unidade> findByOrganogramaId(Long organogramaId);

    @Query("SELECT u FROM Unidade u WHERE u.nome LIKE %:keyword%")
    List<Unidade> searchByKeyword(@Param("keyword") String keyword);


}
