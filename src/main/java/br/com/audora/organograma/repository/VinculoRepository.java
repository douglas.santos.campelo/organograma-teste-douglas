package br.com.audora.organograma.repositories;

import br.com.audora.organograma.models.Vinculo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface VinculoRepository extends JpaRepository<Vinculo, Long> {

    List<Vinculo> findByPessoaId(Long pessoaId);

    @Query("SELECT v FROM Vinculo v WHERE v.pessoa.nome LIKE %:keyword%")
    List<Vinculo> searchByKeyword(@Param("keyword") String keyword);

}
