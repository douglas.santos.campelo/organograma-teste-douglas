package br.com.audora.organograma.repositories;

import br.com.audora.organograma.models.Organograma;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface OrganogramaRepository extends JpaRepository<Organograma, Long> {

    List<Organograma> findByAtivaTrue();

    Optional<Organograma> findByIdAndAtivaTrue(Long id);

    @Query("SELECT o FROM Organograma o WHERE o.nome LIKE %:keyword%")
    List<Organograma> searchByKeyword(@Param("keyword") String keyword);

 
}
