package br.com.audora.organograma.dtos;

public class CriarOrganogramaDTO {
    private boolean ativa;

    public boolean isAtiva() {
        return ativa;
    }

    public void setAtiva(boolean ativa) {
        this.ativa = ativa;
    }

}
