package br.com.audora.organograma.dtos;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ConsultarUnidadeDTO {
    private Long id;
    private String nome;
    private List<ConsultarPessoaPorNomeDTO> pessoas;

    public ConsultarUnidadeDTO(Long id, String nome, List<ConsultarPessoaPorNomeDTO> pessoas) {
        this.id = id;
        this.nome = nome;
        this.pessoas = pessoas;
    }
}
