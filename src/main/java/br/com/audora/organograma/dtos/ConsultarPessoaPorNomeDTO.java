package br.com.audora.organograma.dtos;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@SuppressWarnings("ALL")
@Getter
@Setter
public class ConsultarPessoaPorNomeDTO {
    
    Unidade unidade;
    
    @Getter
    @Setter
    @EqualsAndHashCode
    public static class Unidade {
        long id;
        String descricao;
        List<PessoaDepartamentoFuncao> pessoaDepartamentoFuncaos;

        public Unidade(long id, String descricao) {
            this.id = id;
            this.descricao = descricao;
        }
    }
    
    @Getter
    @Setter
    @AllArgsConstructor
    public static class PessoaDepartamentoFuncao {
        Pessoa pessoa;
        Departamento departamento;
        Funcao funcao;
    }
    
    @Getter
    @Setter
    @AllArgsConstructor
    public static class Pessoa {
        long id;
        String descricao;
    }
    
    @Getter
    @Setter
    @AllArgsConstructor
    public static class Departamento {
        long id;
        String descricao;
    }
    
    @Getter
    @Setter
    @AllArgsConstructor
    public static class Funcao {
        long id;
        String descricao;
    }
}
