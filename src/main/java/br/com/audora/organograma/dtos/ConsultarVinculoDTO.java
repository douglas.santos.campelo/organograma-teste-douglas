package br.com.audora.organograma.dtos;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ConsultarVinculoDTO {
    private Long id;
    private String descricao;
    private List<PessoaDTO> pessoas;

    public ConsultarVinculoDTO(Long id, String descricao, List<PessoaDTO> pessoas) {
        this.id = id;
        this.descricao = descricao;
        this.pessoas = pessoas;
    }

    @Getter
    @Setter
    public static class PessoaDTO {
        private Long id;
        private String descricao;

        public PessoaDTO(Long id, String descricao) {
            this.id = id;
            this.descricao = descricao;
        }
    }
}
