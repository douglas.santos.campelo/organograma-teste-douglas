import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import br.com.audora.organograma.dtos.UnidadeDTO;
import br.com.audora.organograma.models.Unidade;
import br.com.audora.organograma.services.UnidadeService;

import java.util.List;

@RestController
@RequestMapping("/organograma/unidades")
public class UnidadeController {

    private final UnidadeService unidadeService;

    @Autowired
    public UnidadeController(UnidadeService unidadeService) {
        this.unidadeService = unidadeService;
    }

    @GetMapping
    public ResponseEntity<List<UnidadeDTO>> listarUnidades() {
        List<UnidadeDTO> unidades = unidadeService.listarUnidades();
        return ResponseEntity.ok(unidades);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UnidadeDTO> consultarUnidadePorId(@PathVariable Long id) {
        UnidadeDTO unidade = unidadeService.consultarUnidadePorId(id);
        if (unidade != null) {
            return ResponseEntity.ok(unidade);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<UnidadeDTO> cadastrarUnidade(@RequestBody UnidadeDTO unidadeDTO) {
        UnidadeDTO novaUnidade = unidadeService.cadastrarUnidade(unidadeDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(novaUnidade);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UnidadeDTO> atualizarUnidade(@PathVariable Long id, @RequestBody UnidadeDTO unidadeDTO) {
        UnidadeDTO unidadeAtualizada = unidadeService.atualizarUnidade(id, unidadeDTO);
        if (unidadeAtualizada != null) {
            return ResponseEntity.ok(unidadeAtualizada);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> excluirUnidade(@PathVariable Long id) {
        boolean sucesso = unidadeService.excluirUnidade(id);
        if (sucesso) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}