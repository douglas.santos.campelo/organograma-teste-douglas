import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.com.audora.organograma.dtos.ConsultarPessoaPorNomeDTO;
import br.com.audora.organograma.services.OrganogramaService;

@RestController
@RequestMapping("/organograma")
public class OrganogramaController {

    private final OrganogramaService organogramaService;

    @Autowired
    public OrganogramaController(OrganogramaService organogramaService) {
        this.organogramaService = organogramaService;
    }

    @GetMapping("/pessoas/consultar-por-nome/{nome:.*}")
    public ResponseEntity<List<ConsultarPessoaPorNomeDTO>> consultarPessoasPorNome(@PathVariable("nome") String nome) {
        List<ConsultarPessoaPorNomeDTO> pessoas = organogramaService.consultarPessoasPorNome(nome);
        return ResponseEntity.ok(pessoas);
    }

}