import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import br.com.audora.organograma.models.Funcao;
import br.com.audora.organograma.services.FuncaoService;

import java.util.List;

@RestController
@RequestMapping("/organograma/funcoes")
public class FuncaoController {

    @Autowired
    private FuncaoService funcaoService;

    @GetMapping("/{id}")
    public ResponseEntity<Funcao> getFuncao(@PathVariable Long id) {
        Funcao funcao = funcaoService.getFuncaoById(id);
        if (funcao != null) {
            return ResponseEntity.ok(funcao);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<Funcao> createFuncao(@RequestBody Funcao funcao) {
        Funcao createdFuncao = funcaoService.createFuncao(funcao);
        return ResponseEntity.created(createdFuncao.getId()).body(createdFuncao);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Funcao> updateFuncao(@PathVariable Long id, @RequestBody Funcao funcao) {
        Funcao updatedFuncao = funcaoService.updateFuncao(id, funcao);
        if (updatedFuncao != null) {
            return ResponseEntity.ok(updatedFuncao);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteFuncao(@PathVariable Long id) {
        funcaoService.deleteFuncao(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<Funcao>> getAllFuncoes() {
        List<Funcao> funcoes = funcaoService.getAllFuncoes();
        return ResponseEntity.ok(funcoes);
    }

    @GetMapping("/descricao/{descricao}")
    public ResponseEntity<List<Funcao>> getFuncoesPorDescricao(@PathVariable String descricao) {
        List<Funcao> funcoes = funcaoService.getFuncoesPorDescricao(descricao);
        return ResponseEntity.ok(funcoes);
    }

    // Outros métodos, como listar funções por critérios específicos, etc.
}