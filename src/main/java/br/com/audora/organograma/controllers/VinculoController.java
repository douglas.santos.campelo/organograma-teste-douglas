import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import br.com.audora.organograma.dtos.VinculoDTO;
import br.com.audora.organograma.models.Vinculo;
import br.com.audora.organograma.services.VinculoService;

import java.util.List;

@RestController
@RequestMapping("/organograma/vinculos")
public class VinculoController {

    private final VinculoService vinculoService;

    @Autowired
    public VinculoController(VinculoService vinculoService) {
        this.vinculoService = vinculoService;
    }

    @GetMapping
    public ResponseEntity<List<VinculoDTO>> listarVinculos() {
        List<VinculoDTO> vinculos = vinculoService.listarVinculos();
        return ResponseEntity.ok(vinculos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<VinculoDTO> consultarVinculoPorId(@PathVariable Long id) {
        VinculoDTO vinculo = vinculoService.consultarVinculoPorId(id);
        if (vinculo != null) {
            return ResponseEntity.ok(vinculo);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<VinculoDTO> cadastrarVinculo(@RequestBody VinculoDTO vinculoDTO) {
        VinculoDTO novoVinculo = vinculoService.cadastrarVinculo(vinculoDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(novoVinculo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<VinculoDTO> atualizarVinculo(@PathVariable Long id, @RequestBody VinculoDTO vinculoDTO) {
        VinculoDTO vinculoAtualizado = vinculoService.atualizarVinculo(id, vinculoDTO);
        if (vinculoAtualizado != null) {
            return ResponseEntity.ok(vinculoAtualizado);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> excluirVinculo(@PathVariable Long id) {
        boolean sucesso = vinculoService.excluirVinculo(id);
        if (sucesso) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}