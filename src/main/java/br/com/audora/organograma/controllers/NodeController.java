import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import br.com.audora.organograma.models.Node;
import br.com.audora.organograma.services.NodeService;

import java.util.List;

@RestController
@RequestMapping("/organograma/nodes")
public class NodeController {

    @Autowired
    private NodeService nodeService;

    @GetMapping("/{id}")
    public ResponseEntity<Node> getNode(@PathVariable Long id) {
        Node node = nodeService.getNodeById(id);
        if (node != null) {
            return ResponseEntity.ok(node);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<Node> createNode(@RequestBody Node node) {
        Node createdNode = nodeService.createNode(node);
        return ResponseEntity.created(createdNode.getId()).body(createdNode);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Node> updateNode(@PathVariable Long id, @RequestBody Node node) {
        Node updatedNode = nodeService.updateNode(id, node);
        if (updatedNode != null) {
            return ResponseEntity.ok(updatedNode);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteNode(@PathVariable Long id) {
        nodeService.deleteNode(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<Node>> getAllNodes() {
        List<Node> nodes = nodeService.getAllNodes();
        return ResponseEntity.ok(nodes);
    }

    // Outros métodos, como listar nodes por critérios específicos, etc.
}