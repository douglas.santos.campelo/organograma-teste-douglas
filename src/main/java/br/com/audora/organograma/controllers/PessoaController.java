package br.com.audora.organograma.controllers;

import org.hibernate.mapping.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.audora.organograma.services.PessoaService;

public class PessoaController{


    private final PessoaService pessoaService;


@Autowired
public PessoaController(PessoaService pessoaService){
     this.pessoaService = pessoaService;
}

public ResponseEntity<List<ConsultarPessoaPorNomeDTO>> consultarPessoaPorNome(@PathVariable("nome") String nome) {

  List<ConsultarPessoaPorNomeDTO> result = pessoaService.consultarPessoasPorNome(nome);

  return ResponseEntity.ok(result);

   }
}