import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import br.com.audora.organograma.models.Departamento;
import br.com.audora.organograma.services.DepartamentoService;

import java.util.List;

@RestController
@RequestMapping("/organograma/departamentos")
public class DepartamentoController {

    @Autowired
    private DepartamentoService departamentoService;

    @GetMapping("/{id}")
    public ResponseEntity<Departamento> getDepartamento(@PathVariable Long id) {
        Departamento departamento = departamentoService.buscarDepartamentoPorId(id);
        return ResponseEntity.ok(departamento);
    }

    @PostMapping
    public ResponseEntity<Departamento> createDepartamento(@RequestBody Departamento departamento) {
        Departamento novoDepartamento = departamentoService.criarDepartamento(departamento);
        return ResponseEntity.ok(novoDepartamento);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Departamento> updateDepartamento(@PathVariable Long id, @RequestBody Departamento departamento) {
        Departamento departamentoAtualizado = departamentoService.atualizarDepartamento(id, departamento);
        return ResponseEntity.ok(departamentoAtualizado);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteDepartamento(@PathVariable Long id) {
        departamentoService.excluirDepartamento(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<Departamento>> getAllDepartamentos() {
        List<Departamento> departamentos = departamentoService.listarDepartamentos();
        return ResponseEntity.ok(departamentos);
    }

    @GetMapping("/nome/{nome}")
    public ResponseEntity<List<Departamento>> getDepartamentosPorNome(@PathVariable String nome) {
        List<Departamento> departamentos = departamentoService.getDepartamentosPorNome(nome);
        return ResponseEntity.ok(departamentos);
    }

}
