package br.com.audora.organograma.services;

import br.com.audora.organograma.dtos.*;
import br.com.audora.organograma.models.*;
import br.com.audora.organograma.repositories.UnidadeRepository;
import jakarta.persistence.EntityManager;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UnidadeServiceImpl implements UnidadeService {

    private final UnidadeRepository unidadeRepository;
    private final PessoaRepository pessoaRepository;
    private final EntityManager entityManager;

    @Autowired
    public UnidadeServiceImpl(UnidadeRepository unidadeRepository, PessoaRepository pessoaRepository, EntityManager entityManager) {
        this.unidadeRepository = unidadeRepository;
        this.pessoaRepository = pessoaRepository;
        this.entityManager = entityManager;
    }

    @Override
    public ConsultarUnidadeDTO criarUnidade(CriarUnidadeDTO criarUnidadeDTO) {
    Unidade novaUnidade = new Unidade();
    novaUnidade.setNome(criarUnidadeDTO.getNome());
    Unidade unidadeSalva = unidadeRepository.save(novaUnidade);

    return transformarEmConsultarUnidadeDTO(unidadeSalva);
}

@Override
   public ConsultarUnidadeDTO atualizarUnidade(Long unidadeId, AtualizarUnidadeDTO atualizarUnidadeDTO) {
    // Verificar se a unidade existe antes de atualizar
      Unidade unidade = unidadeRepository.findById(unidadeId)
            .orElseThrow(() -> new UnidadeNotFoundException("Unidade não encontrada com o ID: " + unidadeId));

      unidade.setNome(atualizarUnidadeDTO.getNovoNome());
      Unidade unidadeAtualizada = unidadeRepository.save(unidade);

      return transformarEmConsultarUnidadeDTO(unidadeAtualizada);
}

private ConsultarUnidadeDTO transformarEmConsultarUnidadeDTO(Unidade unidade) {
    ConsultarUnidadeDTO consultarUnidadeDTO = new ConsultarUnidadeDTO();
    consultarUnidadeDTO.setId(unidade.getId());
    consultarUnidadeDTO.setNome(unidade.getNome());

    return consultarUnidadeDTO;
}


    @Override
    public void excluirUnidade(Long unidadeId) {
     Unidade unidade = unidadeRepository.findById(unidadeId)
            .orElseThrow(() -> new UnidadeNotFoundException("Unidade não encontrada com o ID: " + unidadeId));

    unidadeRepository.delete(unidade);
    }


    @Override
    public List<ConsultarUnidadeDTO> consultarUnidadesPorNome(String nome) {
    List<Unidade> unidades = unidadeRepository.findByNomeContainingIgnoreCase(nome);

    List<ConsultarUnidadeDTO> dtos = new ArrayList<>();
    for (Unidade unidade : unidades) {
        ConsultarUnidadeDTO dto = transformarEmConsultarUnidadeDTO(unidade);
        dtos.add(dto);
    }

    return dtos;
}


    @Override
public List<ConsultarPessoaPorNomeDTO> consultarPessoasPorNome(String nome) {
    Unidade unidade = unidadeRepository.findByNome(nome);

    if (unidade == null) {
        throw new UnidadeNotFoundException("Unidade não encontrada com o nome: " + nome);
    }

    List<Pessoa> pessoas = unidade.getPessoas();

    List<ConsultarPessoaPorNomeDTO> dtos = new ArrayList<>();
    for (Pessoa pessoa : pessoas) {
        ConsultarPessoaPorNomeDTO dto = transformarEmConsultarPessoaPorNomeDTO(pessoa);
        dtos.add(dto);
    }

    return dtos;
}


}
