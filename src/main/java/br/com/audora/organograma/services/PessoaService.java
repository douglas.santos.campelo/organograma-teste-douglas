package br.com.audora.organograma.services;

import br.com.audora.organograma.dtos.ConsultarPessoaPorNomeDTO;
import java.util.List;

public interface PessoaService {
    List<ConsultarPessoaPorNomeDTO> consultarPessoasPorNome(String nome);
}