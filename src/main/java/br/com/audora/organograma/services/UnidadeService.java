package br.com.audora.organograma.services;

import br.com.audora.organograma.dtos.*;
import br.com.audora.organograma.models.*;

import java.util.List;

public interface UnidadeService {

    ConsultarUnidadeDTO criarUnidade(CriarUnidadeDTO criarUnidadeDTO);

    ConsultarUnidadeDTO atualizarUnidade(Long unidadeId, AtualizarUnidadeDTO atualizarUnidadeDTO);

    void excluirUnidade(Long unidadeId);

    List<ConsultarUnidadeDTO> consultarUnidadesPorNome(String nome);

    List<ConsultarPessoaPorNomeDTO> consultarPessoasPorNome(String nome);
}
