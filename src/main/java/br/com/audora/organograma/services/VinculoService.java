package br.com.audora.organograma.services;

import br.com.audora.organograma.dtos.CriarVinculoDTO;
import br.com.audora.organograma.dtos.ConsultarVinculoDTO;

import java.util.List;

public interface VinculoService {

    ConsultarVinculoDTO criarVinculo(CriarVinculoDTO criarVinculoDTO);

    List<ConsultarVinculoDTO> listarVinculos();

    ConsultarVinculoDTO buscarVinculoPorId(Long vinculoId);

    ConsultarVinculoDTO atualizarVinculo(Long vinculoId, CriarVinculoDTO criarVinculoDTO);

    void excluirVinculo(Long vinculoId);

}
