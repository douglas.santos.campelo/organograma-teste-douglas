package br.com.audora.organograma.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.audora.organograma.dtos.ConsultarNodeDTO;
import br.com.audora.organograma.models.Node;
import br.com.audora.organograma.repositories.NodeRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class NodeServiceImpl implements NodeService {

    private final NodeRepository nodeRepository;

    @Autowired
    public NodeServiceImpl(NodeRepository nodeRepository) {
        this.nodeRepository = nodeRepository;
    }

    @Override
    public List<ConsultarNodeDTO> listarTodosOsNodes() {
        List<Node> nodes = nodeRepository.findAll();
        return nodes.stream()
                .map(this::converterParaConsultarNodeDTO)
                .collect(Collectors.toList());
    }

private ConsultarNodeDTO converterParaConsultarNodeDTO(Node node) {
    ConsultarNodeDTO consultarNodeDTO = new ConsultarNodeDTO();
    consultarNodeDTO.setId(node.getId());
    consultarNodeDTO.setNome(node.getNome());

    return consultarNodeDTO;
}
