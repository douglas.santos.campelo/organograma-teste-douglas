package br.com.audora.organograma.services;

import br.com.audora.organograma.dtos.CriarVinculoDTO;
import br.com.audora.organograma.dtos.ConsultarVinculoDTO;
import br.com.audora.organograma.models.Pessoa;
import br.com.audora.organograma.models.Vinculo;
import br.com.audora.organograma.repositories.PessoaRepository;
import br.com.audora.organograma.repositories.VinculoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class VinculoServiceImpl implements VinculoService {

    private final VinculoRepository vinculoRepository;
    private final PessoaRepository pessoaRepository;

    @Autowired
    public VinculoServiceImpl(VinculoRepository vinculoRepository, PessoaRepository pessoaRepository) {
        this.vinculoRepository = vinculoRepository;
        this.pessoaRepository = pessoaRepository;
    }

    @Override
    public ConsultarVinculoDTO criarVinculo(CriarVinculoDTO criarVinculoDTO) {
    Vinculo novoVinculo = new Vinculo();
    Vinculo vinculoSalvo = vinculoRepository.save(novoVinculo);
    ConsultarVinculoDTO dto = transformarEmConsultarVinculoDTO(vinculoSalvo);

    return dto;
}

    @Override
    public List<ConsultarVinculoDTO> listarVinculos() {
      List<Vinculo> vinculos = vinculoRepository.findAll();
      List<ConsultarVinculoDTO> dtos = new ArrayList<>();

    for (Vinculo vinculo : vinculos) {
        ConsultarVinculoDTO dto = transformarEmConsultarVinculoDTO(vinculo);
        dtos.add(dto);
    }

    return dtos;
}


    @Override
    public ConsultarVinculoDTO buscarVinculoPorId(Long vinculoId) {
        Vinculo vinculo = vinculoRepository.findById(vinculoId)
            .orElseThrow(() -> new VinculoNotFoundException("Vínculo não encontrado com o ID: " + vinculoId));

      return transformarEmConsultarVinculoDTO(vinculo);
}


    @Override
    public ConsultarVinculoDTO atualizarVinculo(Long vinculoId, CriarVinculoDTO criarVinculoDTO) {
        Vinculo vinculoExistente = vinculoRepository.findById(vinculoId)
                .orElseThrow(() -> new VinculoNotFoundException("Vínculo não encontrado com o ID: " + vinculoId));

        vinculoExistente.setCampo1(criarVinculoDTO.getCampo1());
        vinculoExistente.setCampo2(criarVinculoDTO.getCampo2());

        Vinculo vinculoAtualizado = vinculoRepository.save(vinculoExistente);

        return transformarEmConsultarVinculoDTO(vinculoAtualizado);
    }


    @Override
   public void excluirVinculo(Long vinculoId) {
        Vinculo vinculo = vinculoRepository.findById(vinculoId)
                .orElseThrow(() -> new VinculoNotFoundException("Vínculo não encontrado com o ID: " + vinculoId));

  
        vinculoRepository.delete(vinculo);
    }


     private ConsultarVinculoDTO transformarEmConsultarVinculoDTO(Vinculo vinculo) {
        ConsultarVinculoDTO dto = new ConsultarVinculoDTO();
        dto.setId(vinculo.getId());
        dto.setDescricao(vinculo.getDescricao());

        return dto;
    }



    private Pessoa getPessoaPorId(Long pessoaId) {
        return pessoaRepository.findById(pessoaId)
                .orElseThrow(() -> new PessoaNotFoundException("Pessoa não encontrada com o ID: " + pessoaId));
    }

    private List<ConsultarVinculoDTO> transformarEmConsultarVinculoDTOList(List<Vinculo> vinculos) {
        return vinculos.stream()
                .map(this::transformarEmConsultarVinculoDTO)
                .collect(Collectors.toList());
    }
}
