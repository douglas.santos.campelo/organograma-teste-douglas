package br.com.audora.organograma.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.util.Pair;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import br.com.audora.organograma.dtos.ConsultarPessoaPorNomeDTO;
import br.com.audora.organograma.models.Departamento;
import br.com.audora.organograma.models.Node;
import br.com.audora.organograma.models.Pessoa;
import br.com.audora.organograma.models.Unidade;
import br.com.audora.organograma.repositories.PessoaRepository;
import lombok.SneakyThrows;

@Service
public class PessoaServiceImpl implements PessoaService {

    private final PessoaRepository pessoaRepository;

    @Autowired
    public PessoaServiceImpl(PessoaRepository pessoaRepository) {
        this.pessoaRepository = pessoaRepository;
    }

    @Override
    public List<ConsultarPessoaPorNomeDTO> consultarPessoasPorNome(String nome) {
        List<Node> nodes = consultarNodesNoOrganograma(nome);
        return transformarEmConsultarPessoaPorNomeDTO(nodes);
    }

    private List<Node> consultarNodesNoOrganograma(String texto) {
        // Use a PessoaRepository para buscar os nodes relacionados ao nome
        List<Node> nodes = pessoaRepository.consultarNodesPorNome(texto);
        return nodes;
    }


    private List<ConsultarPessoaPorNomeDTO> transformarEmConsultarPessoaPorNomeDTO(List<Node> nodes) {
        Map<ConsultarPessoaPorNomeDTO.Unidade, List<ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao>> map = new HashMap<>();

        for (Node node : nodes) {
            Pair<ConsultarPessoaPorNomeDTO.Unidade, ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao> par = getUnidade(node);
            if (!map.containsKey(par.getKey())) {
                List<ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao> pessoaDepartamentoFuncaos = new ArrayList<>();
                pessoaDepartamentoFuncaos.add(par.getValue());
                map.put(par.getKey(), pessoaDepartamentoFuncaos);
            } else {
                List<ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao> pessoaDepartamentoFuncaos = map.get(par.getKey());
                pessoaDepartamentoFuncaos.add(par.getValue());
                map.put(par.getKey(), pessoaDepartamentoFuncaos);
            }
        }

        List<ConsultarPessoaPorNomeDTO> dto = new ArrayList<>();

        for (Map.Entry<ConsultarPessoaPorNomeDTO.Unidade, List<ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao>> entry : map.entrySet()) {
            entry.getKey().setPessoaDepartamentoFuncaos(entry.getValue());
            ConsultarPessoaPorNomeDTO consultarPessoaPorNomeDTO = new ConsultarPessoaPorNomeDTO();
            consultarPessoaPorNomeDTO.setUnidade(entry.getKey());
            dto.add(consultarPessoaPorNomeDTO);
        }
        return dto;
    }

    @SneakyThrows
    private Pair<ConsultarPessoaPorNomeDTO.Unidade, ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao> getUnidade(Node node) {
        Unidade unidade = getElementoFromNode(node.getNodeParent(), "Unidade");
        Departamento departamento = getElementoFromNode(node.getNodeParent(), "Departamento");
        ConsultarPessoaPorNomeDTO.Funcao funcao = getElementoFromNode(node.getNodeParent(), "Funcao");
        Pessoa pessoa = node.getVinculo().getPessoa();

        ConsultarPessoaPorNomeDTO.Unidade unidadeDTO = new ConsultarPessoaPorNomeDTO.Unidade(unidade.getId(), unidade.getNome());

        ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao pessoaDepartamentoFuncao = new ConsultarPessoaPorNomeDTO.PessoaDepartamentoFuncao(
                criarPessoa(pessoa),
                criarDepartamento(departamento),
                criarFuncao(funcao));
        return Pair.of(unidadeDTO, pessoaDepartamentoFuncao);
    }

    private ConsultarPessoaPorNomeDTO.Funcao criarFuncao(ConsultarPessoaPorNomeDTO.Funcao funcao) {
        return new ConsultarPessoaPorNomeDTO.Funcao(funcao.getId(), funcao.getNome());
    }

    private ConsultarPessoaPorNomeDTO.Departamento criarDepartamento(Departamento departamento) {
        return new ConsultarPessoaPorNomeDTO.Departamento(departamento.getId(), departamento.getNome());
    }

    private ConsultarPessoaPorNomeDTO.Pessoa criarPessoa(Pessoa pessoa) {
        return new ConsultarPessoaPorNomeDTO.Pessoa(pessoa.getId(), pessoa.getNome());
    }

    @SneakyThrows
    @SuppressWarnings("unchecked")
    private <T> T getElementoFromNode(Node node, String mapeamento) {
        T expected = null;
        while (node != null && (expected = (T) node.getClass().getMethod("get" + mapeamento).invoke(node)) == null) {
            node = node.getNodeParent();
        }
        return expected;
    }
}

