package br.com.audora.organograma.services;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.audora.organograma.models.Departamento;
import br.com.audora.organograma.repositories.DepartamentoRepository;

@Service
public class DepartamentoServiceImpl {

    private final DepartamentoRepository departamentoRepository;

    @Autowired
    public DepartamentoService(DepartamentoRepository departamentoRepository) {
        this.departamentoRepository = departamentoRepository;
    }

    public List<Departamento> listarDepartamentos() {
        return departamentoRepository.findAll();
    }

    public Departamento buscarDepartamentoPorId(Long id) {
        return departamentoRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Departamento não encontrado"));
    }

    public Departamento criarDepartamento(Departamento departamento) {
        return departamentoRepository.save(departamento);
    }

    public Departamento atualizarDepartamento(Long id, Departamento novoDepartamento) {
        Departamento departamentoExistente = buscarDepartamentoPorId(id);
        
        // Atualize os atributos conforme necessário
        departamentoExistente.setNome(novoDepartamento.getNome());
        // Atualize outros atributos
        
        return departamentoRepository.save(departamentoExistente);
    }

    public void excluirDepartamento(Long id) {
        Departamento departamento = buscarDepartamentoPorId(id);
        departamentoRepository.delete(departamento);
    }
}
