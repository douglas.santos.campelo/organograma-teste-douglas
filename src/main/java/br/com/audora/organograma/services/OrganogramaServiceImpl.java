package br.com.audora.organograma.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.com.audora.organograma.dtos.*;
import br.com.audora.organograma.models.*;
import br.com.audora.organograma.repositories.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class OrganogramaServiceImpl implements OrganogramaService {

    private final OrganogramaRepository organogramaRepository;
    private final UnidadeRepository unidadeRepository;
    private final PessoaRepository pessoaRepository;
    private final VinculoRepository vinculoRepository;

    @Autowired
    public OrganogramaServiceImpl(OrganogramaRepository organogramaRepository, UnidadeRepository unidadeRepository,
                                  PessoaRepository pessoaRepository, VinculoRepository vinculoRepository) {
        this.organogramaRepository = organogramaRepository;
        this.unidadeRepository = unidadeRepository;
        this.pessoaRepository = pessoaRepository;
        this.vinculoRepository = vinculoRepository;
    }

    @Override
    public OrganogramaDTO criarOrganograma(CriarOrganogramaDTO criarOrganogramaDTO) {
        Organograma novoOrganograma = new Organograma();
        novoOrganograma.setAtiva(true);
        Organograma organogramaSalvo = organogramaRepository.save(novoOrganograma);

        return transformarEmOrganogramaDTO(organogramaSalvo);
    }

    private OrganogramaDTO transformarEmOrganogramaDTO(Organograma organograma) {
        OrganogramaDTO organogramaDTO = new OrganogramaDTO();
        organogramaDTO.setId(organograma.getId());
        organogramaDTO.setAtiva(organograma.getAtiva());
        // Outros campos do DTO

        return organogramaDTO;
    }

    @Override
    public OrganogramaDTO atualizarOrganograma(Long organogramaId, CriarOrganogramaDTO criarOrganogramaDTO) {
        Organograma organogramaExistente = organogramaRepository.findById(organogramaId)
                .orElseThrow(() -> new OrganogramaNotFoundException("Organograma não encontrado com o ID: " + organogramaId));

        organogramaExistente.setAtiva(criarOrganogramaDTO.isAtiva());
        // Outros campos a serem atualizados

        Organograma organogramaAtualizado = organogramaRepository.save(organogramaExistente);

        return transformarEmOrganogramaDTO(organogramaAtualizado);
    }

    @Override
    public void excluirOrganograma(Long organogramaId) {
        Organograma organograma = organogramaRepository.findById(organogramaId)
                .orElseThrow(() -> new OrganogramaNotFoundException("Organograma não encontrado com o ID: " + organogramaId));

        organogramaRepository.delete(organograma);
    }

    @Override
    public List<ConsultarUnidadeDTO> consultarUnidadesPorNome(String nome) {
        List<Unidade> unidades = unidadeRepository.findByNomeContaining(nome);
        List<ConsultarUnidadeDTO> consultarUnidadeDTOs = new ArrayList<>();

        for (Unidade unidade : unidades) {
            ConsultarUnidadeDTO consultarUnidadeDTO = new ConsultarUnidadeDTO();
            consultarUnidadeDTO.setId(unidade.getId());
            consultarUnidadeDTO.setNome(unidade.getNome());

            consultarUnidadeDTOs.add(consultarUnidadeDTO);
        }

        return consultarUnidadeDTOs;
    }

    @Override
    public List<ConsultarPessoaPorNomeDTO> consultarPessoasPorNome(String nome) {
        List<Node> nodes = consultarNodesNoOrganograma(nome);
        List<ConsultarPessoaPorNomeDTO> consultarPessoasDTO = transformarEmConsultarPessoaPorNomeDTO(nodes);

        return consultarPessoasDTO;
    }

    @Transactional
    @Override
    public ConsultarPessoaPorNomeDTO criarPessoa(CriarPessoaDTO criarPessoaDTO) {
        Unidade unidade = unidadeRepository.findById(criarPessoaDTO.getUnidadeId())
                .orElseThrow(() -> new UnidadeNotFoundException("Unidade não encontrada com o ID: " + criarPessoaDTO.getUnidadeId()));

        Pessoa novaPessoa = new Pessoa();
        novaPessoa.setNome(criarPessoaDTO.getNome());

        // Outras configurações para a nova pessoa, se necessário

        Vinculo novoVinculo = new Vinculo();
        novoVinculo.setPessoa(novaPessoa);
        novoVinculo.setUnidade(unidade);

        // Outras configurações para o novo vínculo, se necessário

        vinculoRepository.save(novoVinculo);

        ConsultarPessoaPorNomeDTO consultarPessoaPorNomeDTO = new ConsultarPessoaPorNomeDTO();
        // Configure as propriedades do consultarPessoaPorNomeDTO com base na nova pessoa e no vínculo criado

        return consultarPessoaPorNomeDTO;
    }

    @Transactional
    @Override
    public ConsultarPessoaPorNomeDTO atualizarPessoa(Long pessoaId, AtualizarPessoaDTO atualizarPessoaDTO) {
        Pessoa pessoaExistente = pessoaRepository.findById(pessoaId)
                .orElseThrow(() -> new PessoaNotFoundException("Pessoa não encontrada com o ID: " + pessoaId));

        pessoaExistente.setNome(atualizarPessoaDTO.getNome());

        // Outras atualizações para a pessoa existente, se necessário

        ConsultarPessoaPorNomeDTO consultarPessoaPorNomeDTO = new ConsultarPessoaPorNomeDTO();
        // Configure as propriedades do consultarPessoaPorNomeDTO com base na pessoa existente e nos seus vínculos

        return consultarPessoaPorNomeDTO;
    }

    @Override
    public void excluirPessoa(Long pessoaId) {
        Pessoa pessoa = pessoaRepository.findById(pessoaId)
                .orElseThrow(() -> new PessoaNotFoundException("Pessoa não encontrada com o ID: " + pessoaId));

        pessoaRepository.delete(pessoa);
    }

    // Outros métodos da interface OrganogramaService

    private List<Node> consultarNodesNoOrganograma(String nome) {
        // Implemente a lógica para consultar os nodes no organograma com base no nome
        // Retorne uma lista de nodes
        return new ArrayList<>();
    }

    private List<ConsultarPessoaPorNomeDTO> transformarEmConsultarPessoaPorNomeDTO(List<Node> nodes) {
        // Implemente a lógica para transformar nodes em DTOs de pessoas
        // Retorne uma lista de DTOs de pessoas
        return new ArrayList<>();
    }

    // Outros métodos auxiliares

}
