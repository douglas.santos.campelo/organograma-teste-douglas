package br.com.audora.organograma.services;

import br.com.audora.organograma.dtos.*;

import java.util.List;

public interface OrganogramaService {
    OrganogramaDTO criarOrganograma(CriarOrganogramaDTO criarOrganogramaDTO);

    OrganogramaDTO atualizarOrganograma(Long organogramaId, CriarOrganogramaDTO criarOrganogramaDTO);

    void excluirOrganograma(Long organogramaId);

    List<ConsultarUnidadeDTO> consultarUnidadesPorNome(String nome);

    List<ConsultarPessoaPorNomeDTO> consultarPessoasPorNome(String nome);

    ConsultarUnidadeDTO criarUnidade(CriarUnidadeDTO criarUnidadeDTO);

    ConsultarUnidadeDTO atualizarUnidade(Long unidadeId, AtualizarUnidadeDTO atualizarUnidadeDTO);

    void excluirUnidade(Long unidadeId);

    ConsultarPessoaPorNomeDTO criarPessoa(CriarPessoaDTO criarPessoaDTO);

    ConsultarPessoaPorNomeDTO atualizarPessoa(Long pessoaId, AtualizarPessoaDTO atualizarPessoaDTO);

    void excluirPessoa(Long pessoaId);

}