package br.com.audora.organograma.services;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.audora.organograma.models.Funcao;
import br.com.audora.organograma.repositories.FuncaoRepository;

@Service
public class FuncaoService {

    private final FuncaoRepository funcaoRepository;

    @Autowired
    public FuncaoService(FuncaoRepository funcaoRepository) {
        this.funcaoRepository = funcaoRepository;
    }

    public List<Funcao> listarFuncoes() {
        return funcaoRepository.findAll();
    }

    public Funcao buscarFuncaoPorId(Long id) {
        return funcaoRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Função não encontrada"));
    }

    public Funcao criarFuncao(Funcao funcao) {
        return funcaoRepository.save(funcao);
    }

    public Funcao atualizarFuncao(Long id, Funcao novaFuncao) {
        Funcao funcaoExistente = buscarFuncaoPorId(id);
        
        funcaoExistente.setNome(novaFuncao.getNome());

        
        return funcaoRepository.save(funcaoExistente);
    }

    public void excluirFuncao(Long id) {
        Funcao funcao = buscarFuncaoPorId(id);
        funcaoRepository.delete(funcao);
    }
}
