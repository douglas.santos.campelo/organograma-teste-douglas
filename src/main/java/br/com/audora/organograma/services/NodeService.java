package br.com.audora.organograma.services;

import br.com.audora.organograma.dtos.ConsultarNodeDTO;

import java.util.List;

public interface NodeService {
    List<ConsultarNodeDTO> listarTodosOsNodes();

}
