package br.com.audora.organograma.services;

import java.util.List;

import br.com.audora.organograma.models.Departamento;

public interface DepartamentoService {
    List<Departamento> listarDepartamentos();
    Departamento buscarDepartamentoPorId(Long id);
    Departamento criarDepartamento(Departamento departamento);
    Departamento atualizarDepartamento(Long id, Departamento departamento);
    void excluirDepartamento(Long id);
}