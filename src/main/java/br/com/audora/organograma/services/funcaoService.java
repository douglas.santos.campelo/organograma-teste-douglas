package br.com.audora.organograma.services;

import java.util.List;

import br.com.audora.organograma.models.Funcao;

public interface FuncaoService {

    List<Funcao> listarFuncoes();

    Funcao buscarFuncaoPorId(Long id);

    Funcao criarFuncao(Funcao funcao);

    Funcao atualizarFuncao(Long id, Funcao funcao);

    void excluirFuncao(Long id);
}
