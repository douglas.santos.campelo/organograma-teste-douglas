create table vinculo(
    id bigint primary key auto_increment,
    pessoa_id bigint not null,

    foreign key (pessoa_id) references pessoa(id)
);