package br.com.audora.organograma.services;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.audora.organograma.dtos.CriarOrganogramaDTO;
import br.com.audora.organograma.dtos.AtualizarOrganogramaDTO;
import br.com.audora.organograma.dtos.ConsultarUnidadeDTO;
import br.com.audora.organograma.dtos.ConsultarPessoaPorNomeDTO;
import br.com.audora.organograma.models.Organograma;
import br.com.audora.organograma.repositories.OrganogramaRepository;
import br.com.audora.organograma.repositories.UnidadeRepository;
import br.com.audora.organograma.repositories.PessoaRepository;

@RunWith(MockitoJUnitRunner.class)
public class OrganogramaServiceTest {
	@Mock
	private OrganogramaRepository organogramaRepository;
	@Mock
	private UnidadeRepository unidadeRepository;
	@Mock
	private PessoaRepository pessoaRepository;

	private OrganogramaService organogramaService;

	@Before
	public void setup() {
		this.organogramaService = new OrganogramaService(organogramaRepository, unidadeRepository, pessoaRepository);
	}

	@Test
	public void shouldCriarOrganograma() {
		CriarOrganogramaDTO criarOrganogramaDTO = new CriarOrganogramaDTO();

		Organograma organograma = new Organograma();
		when(organogramaRepository.save(any(Organograma.class))).thenReturn(organograma);

		OrganogramaDTO actualValue = organogramaService.criarOrganograma(criarOrganogramaDTO);

		assertThat(actualValue, notNullValue());
	}

	@Test
	public void shouldAtualizarOrganograma() {
		Long organogramaId = 1L;
		AtualizarOrganogramaDTO atualizarOrganogramaDTO = new AtualizarOrganogramaDTO();

		Organograma organogramaExistente = new Organograma();
		when(organogramaRepository.findById(organogramaId)).thenReturn(java.util.Optional.of(organogramaExistente));

		Organograma organogramaAtualizado = new Organograma();
		when(organogramaRepository.save(any(Organograma.class))).thenReturn(organogramaAtualizado);

		OrganogramaDTO actualValue = organogramaService.atualizarOrganograma(organogramaId, atualizarOrganogramaDTO);

		assertThat(actualValue, notNullValue());
	}

}
