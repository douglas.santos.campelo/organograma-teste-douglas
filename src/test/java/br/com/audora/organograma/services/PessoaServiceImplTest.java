import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.util.Pair;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import br.com.audora.organograma.dtos.ConsultarPessoaPorNomeDTO;
import br.com.audora.organograma.models.Departamento;
import br.com.audora.organograma.models.Node;
import br.com.audora.organograma.models.Pessoa;
import br.com.audora.organograma.models.Unidade;
import br.com.audora.organograma.repositories.PessoaRepository;
import lombok.SneakyThrows;

@RunWith(MockitoJUnitRunner.class)
public class PessoaServiceImplTest {
	@Mock
	private PessoaRepository pessoaRepository;

	private PessoaServiceImpl pessoaServiceImpl;

	@Before
	public void setup() {
		this.pessoaServiceImpl = new PessoaServiceImpl(pessoaRepository);
	}

	@Test
	public void shouldConsultarPessoasPorNome() {
		String nome = "John Doe";

		List<Pessoa> pessoas = new ArrayList<>();
		Pessoa pessoa1 = new Pessoa();
		pessoa1.setId(1L);
		pessoa1.setNome("John Doe");
		pessoas.add(pessoa1);

		when(pessoaRepository.findByNomeIgnoreCaseContaining(nome)).thenReturn(pessoas);

		List<ConsultarPessoaPorNomeDTO> actualValue = pessoaServiceImpl.consultarPessoasPorNome(nome);

		assertThat(actualValue, notNullValue());
		assertEquals(1, actualValue.size());

		ConsultarPessoaPorNomeDTO pessoaDto = actualValue.get(0);
		assertThat(pessoaDto.getId(), equalTo(1L));
		assertThat(pessoaDto.getNome(), equalTo("John Doe"));
	}
}
