import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.audora.organograma.dtos.CriarVinculoDTO;
import br.com.audora.organograma.dtos.ConsultarVinculoDTO;
import br.com.audora.organograma.models.Vinculo;
import br.com.audora.organograma.repositories.VinculoRepository;

@RunWith(MockitoJUnitRunner.class)
public class VinculoServiceImplTest {

    @Mock
    private VinculoRepository vinculoRepository;

    private VinculoServiceImpl vinculoService;

    @Before
    public void setup() {
        vinculoService = new VinculoServiceImpl(vinculoRepository);
    }

    @Test
    public void shouldCriarVinculo() {
        CriarVinculoDTO criarVinculoDTO = new CriarVinculoDTO();

        Vinculo vinculoSalvo = new Vinculo();
        when(vinculoRepository.save(any(Vinculo.class))).thenReturn(vinculoSalvo);

        ConsultarVinculoDTO actualValue = vinculoService.criarVinculo(criarVinculoDTO);

    }

    @Test
    public void shouldListarVinculos() {
        List<Vinculo> listaDeVinculos = new ArrayList<>();

        when(vinculoRepository.findAll()).thenReturn(listaDeVinculos);

        List<ConsultarVinculoDTO> actualValue = vinculoService.listarVinculos();

        assertThat(actualValue, notNullValue());
    }

    @Test
    public void shouldBuscarVinculoPorId() {
        Long vinculoId = 1L;
        Vinculo vinculo = new Vinculo();
        when(vinculoRepository.findById(vinculoId)).thenReturn(Optional.of(vinculo));

        ConsultarVinculoDTO actualValue = vinculoService.buscarVinculoPorId(vinculoId);

        assertThat(actualValue, notNullValue());
    }

    @Test
    public void shouldAtualizarVinculo() {
        Long vinculoId = 1L;
        CriarVinculoDTO criarVinculoDTO = new CriarVinculoDTO();

        Vinculo vinculoExistente = new Vinculo();
        when(vinculoRepository.findById(vinculoId)).thenReturn(Optional.of(vinculoExistente));

        ConsultarVinculoDTO actualValue = vinculoService.atualizarVinculo(vinculoId, criarVinculoDTO);

        assertThat(actualValue, notNullValue());
    }

    @Test
    public void shouldExcluirVinculo() {
        Long vinculoId = 1L;
        Vinculo vinculo = new Vinculo();
        when(vinculoRepository.findById(vinculoId)).thenReturn(Optional.of(vinculo));

        vinculoService.excluirVinculo(vinculoId);

        verify(vinculoRepository, times(1)).delete(vinculo);
    }
}
