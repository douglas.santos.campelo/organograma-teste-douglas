import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.audora.organograma.services.VinculoService;

@RunWith(MockitoJUnitRunner.class)
public class VinculoServiceTest {
    @Mock
    private VinculoRepository vinculoRepository; 

    private VinculoService cut;

    @Before
    public void setup() {
        this.cut = new VinculoService(vinculoRepository);
    }

    @Test
    public void shouldCompile() {
        assertThat("Actual value", is("Expected value"));
    }

    @Test
    public void shouldCreateVinculo() {
       
        CriarVinculoDTO criarVinculoDTO = new CriarVinculoDTO();
        Vinculo vinculoSalvo = new Vinculo();
        when(vinculoRepository.save(any(Vinculo.class))).thenReturn(vinculoSalvo);

     
        ConsultarVinculoDTO actualValue = cut.criarVinculo(criarVinculoDTO);

        
        assertThat(actualValue, notNullValue());
       
    }

    
}