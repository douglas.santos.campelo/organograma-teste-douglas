import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.com.audora.organograma.dtos.ConsultarNodeDTO;
import br.com.audora.organograma.models.Node;
import br.com.audora.organograma.repositories.NodeRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(MockitoJUnitRunner.class)
public class NodeServiceImplTest {
	@Mock
	private NodeRepository nodeRepository;

	private NodeServiceImpl nodeServiceImpl;

	@Before
	public void setup() {
		this.nodeServiceImpl = new NodeServiceImpl(nodeRepository);
	}

	@Test
	public void shouldListarTodosOsNodes() {
		List<Node> nodes = new ArrayList<>();
		Node node1 = new Node();
		node1.setId(1L);
		node1.setNome("Node 1");
		nodes.add(node1);

		when(nodeRepository.findAll()).thenReturn(nodes);

		List<ConsultarNodeDTO> actualValue = nodeServiceImpl.listarTodosOsNodes();

		assertThat(actualValue, notNullValue());
		assertEquals(1, actualValue.size());

		ConsultarNodeDTO nodeDto = actualValue.get(0);
		assertThat(nodeDto.getId(), equalTo(1L));
		assertThat(nodeDto.getNome(), equalTo("Node 1"));
	}
}
