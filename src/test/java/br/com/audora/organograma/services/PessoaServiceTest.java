package br.com.audora.organograma.services;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.audora.organograma.dtos.CriarPessoaDTO;
import br.com.audora.organograma.dtos.AtualizarPessoaDTO;
import br.com.audora.organograma.dtos.ConsultarPessoaPorNomeDTO;
import br.com.audora.organograma.models.Pessoa;
import br.com.audora.organograma.repositories.PessoaRepository;

@RunWith(MockitoJUnitRunner.class)
public class PessoaServiceTest {
	@Mock
	private PessoaRepository pessoaRepository;

	private PessoaService pessoaService;

	@Before
	public void setup() {
		this.pessoaService = new PessoaService(pessoaRepository);
	}

	@Test
	public void shouldCriarPessoa() {
		CriarPessoaDTO criarPessoaDTO = new CriarPessoaDTO();

		Pessoa pessoa = new Pessoa();
		when(pessoaRepository.save(any(Pessoa.class))).thenReturn(pessoa);

		ConsultarPessoaPorNomeDTO actualValue = pessoaService.criarPessoa(criarPessoaDTO);

		assertThat(actualValue, notNullValue());
	}

	@Test
	public void shouldAtualizarPessoa() {
		Long pessoaId = 1L;
		AtualizarPessoaDTO atualizarPessoaDTO = new AtualizarPessoaDTO();
		Pessoa pessoaExistente = new Pessoa();
		when(pessoaRepository.findById(pessoaId)).thenReturn(java.util.Optional.of(pessoaExistente));

		Pessoa pessoaAtualizada = new Pessoa();
		when(pessoaRepository.save(any(Pessoa.class))).thenReturn(pessoaAtualizada);

		ConsultarPessoaPorNomeDTO actualValue = pessoaService.atualizarPessoa(pessoaId, atualizarPessoaDTO);

		assertThat(actualValue, notNullValue());
	}

	@Test
	public void shouldExcluirPessoa() {
		Long pessoaId = 1L;
		Pessoa pessoa = new Pessoa();
		when(pessoaRepository.findById(pessoaId)).thenReturn(java.util.Optional.of(pessoa));

		pessoaService.excluirPessoa(pessoaId);

		verify(pessoaRepository, times(1)).delete(pessoa);
	}
}
