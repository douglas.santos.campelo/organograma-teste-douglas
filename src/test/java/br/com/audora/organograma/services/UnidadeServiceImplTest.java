import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.audora.organograma.dtos.*;
import br.com.audora.organograma.models.*;
import br.com.audora.organograma.repositories.UnidadeRepository;
import jakarta.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;

@RunWith(MockitoJUnitRunner.class)
public class UnidadeServiceImplTest {
    @Mock
    private UnidadeRepository unidadeRepository;
    @Mock
    private PessoaRepository pessoaRepository;
    @Mock
    private EntityManager entityManager;

    private UnidadeServiceImpl unidadeServiceImpl;

    @Before
    public void setup() {
        this.unidadeServiceImpl = new UnidadeServiceImpl(unidadeRepository, pessoaRepository, entityManager);
    }

    @Test
    public void shouldCriarUnidade() {
        CriarUnidadeDTO criarUnidadeDTO = new CriarUnidadeDTO();
        criarUnidadeDTO.setNome("Nome da Unidade");

        Unidade unidadeSalva = new Unidade();
        unidadeSalva.setId(1L);
        unidadeSalva.setNome(criarUnidadeDTO.getNome());

        when(unidadeRepository.save(any(Unidade.class))).thenReturn(unidadeSalva);

        ConsultarUnidadeDTO actualValue = unidadeServiceImpl.criarUnidade(criarUnidadeDTO);

        assertThat(actualValue, notNullValue());
        assertThat(actualValue.getId(), is(1L));
        assertThat(actualValue.getNome(), is(criarUnidadeDTO.getNome()));
    }

    @Test
    public void shouldAtualizarUnidade() {
        Long unidadeId = 1L;
        AtualizarUnidadeDTO atualizarUnidadeDTO = new AtualizarUnidadeDTO();
        atualizarUnidadeDTO.setNome("Novo Nome da Unidade");

        Unidade unidadeExistente = new Unidade();
        unidadeExistente.setId(unidadeId);
        unidadeExistente.setNome("Nome Antigo da Unidade");

        when(unidadeRepository.findById(unidadeId)).thenReturn(java.util.Optional.of(unidadeExistente));
        when(unidadeRepository.save(any(Unidade.class))).thenReturn(unidadeExistente);

        ConsultarUnidadeDTO actualValue = unidadeServiceImpl.atualizarUnidade(unidadeId, atualizarUnidadeDTO);

        assertThat(actualValue, notNullValue());
        assertThat(actualValue.getId(), is(unidadeId));
        assertThat(actualValue.getNome(), is(atualizarUnidadeDTO.getNome()));
    }

    @Test
    public void shouldExcluirUnidade() {
        Long unidadeId = 1L;
        Unidade unidade = new Unidade();
        unidade.setId(unidadeId);

        when(unidadeRepository.findById(unidadeId)).thenReturn(java.util.Optional.of(unidade));

        unidadeServiceImpl.excluirUnidade(unidadeId);

        verify(unidadeRepository, times(1)).delete(unidade);
    }

    @Test
    public void shouldConsultarUnidadesPorNome() {
        String nome = "Nome da Unidade";
 

        List<ConsultarUnidadeDTO> actualValue = unidadeServiceImpl.consultarUnidadesPorNome(nome);

        assertThat(actualValue, notNullValue());
    }

    @Test
    public void shouldConsultarPessoasPorNome() {
        String nome = "Nome da Pessoa";
  
        List<ConsultarPessoaPorNomeDTO> actualValue = unidadeServiceImpl.consultarPessoasPorNome(nome);

        assertThat(actualValue, notNullValue());
    }
}
