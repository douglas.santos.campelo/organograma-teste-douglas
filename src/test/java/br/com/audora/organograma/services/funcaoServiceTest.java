package br.com.audora.organograma.services;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class FuncaoServiceTest {
    @Mock
    private FuncaoRepository funcaoRepository;

    private FuncaoService funcaoService;

    @Before
    public void setup() {
        this.funcaoService = new FuncaoService(funcaoRepository);
    }

    @Test
    public void shouldListarFuncoes() {
        List<Funcao> funcoes = new ArrayList<>();
        when(funcaoRepository.findAll()).thenReturn(funcoes);

        List<Funcao> actualValue = funcaoService.listarFuncoes();

        assertThat(actualValue, notNullValue());
    }

    @Test
    public void shouldBuscarFuncaoPorId() {
        Long id = 1L;
        Funcao funcao = new Funcao();
        funcao.setId(id);
        when(funcaoRepository.findById(id)).thenReturn(java.util.Optional.of(funcao));

        Funcao actualValue = funcaoService.buscarFuncaoPorId(id);

        assertThat(actualValue, notNullValue());
        assertEquals(id, actualValue.getId());
    }

    @Test
    public void shouldCriarFuncao() {
        Funcao funcao = new Funcao();
        when(funcaoRepository.save(funcao)).thenReturn(funcao);

        Funcao actualValue = funcaoService.criarFuncao(funcao);

        assertThat(actualValue, notNullValue());
        assertEquals(funcao, actualValue);
    }

    @Test
    public void shouldAtualizarFuncao() {
        Long id = 1L;
        Funcao funcaoExistente = new Funcao();
        when(funcaoRepository.findById(id)).thenReturn(java.util.Optional.of(funcaoExistente));

        Funcao novaFuncao = new Funcao();
        novaFuncao.setId(id);
        when(funcaoRepository.save(novaFuncao)).thenReturn(novaFuncao);

        Funcao actualValue = funcaoService.atualizarFuncao(id, novaFuncao);

        assertThat(actualValue, notNullValue());
        assertEquals(id, actualValue.getId());
        assertEquals(novaFuncao, actualValue);
    }

    @Test
    public void shouldExcluirFuncao() {
        Long id = 1L;
        Funcao funcao = new Funcao();
        when(funcaoRepository.findById(id)).thenReturn(java.util.Optional.of(funcao));

        funcaoService.excluirFuncao(id);

        verify(funcaoRepository, times(1)).delete(funcao);
    }
}
