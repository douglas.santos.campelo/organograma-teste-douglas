import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.audora.organograma.services.UnidadeService;

@RunWith(MockitoJUnitRunner.class)
public class UnidadeServiceTest {
    @Mock
    private UnidadeRepository unidadeRepository; 

    private UnidadeService cut;

    @Before
    public void setup() {
        this.cut = new UnidadeService(unidadeRepository);
    }

    @Test
    public void shouldCompile() {
        assertThat("Actual value", is("Expected value"));
    }


    @Test
    public void shouldCreateUnidade() {
        CriarUnidadeDTO criarUnidadeDTO = new CriarUnidadeDTO();
        Unidade unidadeSalva = new Unidade();
        when(unidadeRepository.save(any(Unidade.class))).thenReturn(unidadeSalva);

        ConsultarUnidadeDTO actualValue = cut.criarUnidade(criarUnidadeDTO);

       
        assertThat(actualValue, notNullValue());
        
    }

    

}
