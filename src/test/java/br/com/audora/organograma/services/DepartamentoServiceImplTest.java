import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RunWith(MockitoJUnitRunner.class)
public class DepartamentoServiceImplTest {
	@Mock
	private DepartamentoRepository departamentoRepository;

	private DepartamentoService departamentoService;

	@Before
	public void setup() {
		this.departamentoService = new DepartamentoService(departamentoRepository);
	}

	@Test
	public void shouldListarDepartamentos() {
		List<Departamento> departamentos = new ArrayList<>();
		Departamento departamento1 = new Departamento();
		departamento1.setId(1L);
		departamento1.setNome("Departamento 1");
		departamentos.add(departamento1);

		when(departamentoRepository.findAll()).thenReturn(departamentos);

		List<Departamento> actualValue = departamentoService.listarDepartamentos();

		assertThat(actualValue, notNullValue());
		assertEquals(1, actualValue.size());

		Departamento departamento = actualValue.get(0);
		assertThat(departamento.getId(), equalTo(1L));
		assertThat(departamento.getNome(), equalTo("Departamento 1"));
	}

	@Test
	public void shouldBuscarDepartamentoPorId() {
		Long id = 1L;
		Departamento departamento = new Departamento();
		departamento.setId(id);
		when(departamentoRepository.findById(id)).thenReturn(java.util.Optional.of(departamento));

		Departamento actualValue = departamentoService.buscarDepartamentoPorId(id);

		assertThat(actualValue, notNullValue());
		assertEquals(id, actualValue.getId());
	}

	@Test
	public void shouldCriarDepartamento() {
		Departamento departamento = new Departamento();
		when(departamentoRepository.save(departamento)).thenReturn(departamento);

		Departamento actualValue = departamentoService.criarDepartamento(departamento);

		assertThat(actualValue, notNullValue());
		assertEquals(departamento, actualValue);
	}

	@Test
	public void shouldAtualizarDepartamento() {
		Long id = 1L;
		Departamento departamentoExistente = new Departamento();
		when(departamentoRepository.findById(id)).thenReturn(java.util.Optional.of(departamentoExistente));

		Departamento novoDepartamento = new Departamento();
		novoDepartamento.setId(id);
		when(departamentoRepository.save(novoDepartamento)).thenReturn(novoDepartamento);

		Departamento actualValue = departamentoService.atualizarDepartamento(id, novoDepartamento);

		assertThat(actualValue, notNullValue());
		assertEquals(id, actualValue.getId());
		assertEquals(novoDepartamento, actualValue);
	}

	@Test
	public void shouldExcluirDepartamento() {
		Long id = 1L;
		Departamento departamento = new Departamento();
		when(departamentoRepository.findById(id)).thenReturn(java.util.Optional.of(departamento));

		departamentoService.excluirDepartamento(id);

		verify(departamentoRepository, times(1)).delete(departamento);
	}
}
