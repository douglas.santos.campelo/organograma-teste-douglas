import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.com.audora.organograma.dtos.*;
import br.com.audora.organograma.models.*;
import br.com.audora.organograma.repositories.*;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class OrganogramaServiceImplTest {
	@Mock
	private OrganogramaRepository organogramaRepository;
	@Mock
	private UnidadeRepository unidadeRepository;
	@Mock
	private PessoaRepository pessoaRepository;
	@Mock
	private VinculoRepository vinculoRepository;

	private OrganogramaServiceImpl organogramaServiceImpl;

	@Before
	public void setup() {
		this.organogramaServiceImpl = new OrganogramaServiceImpl(organogramaRepository, unidadeRepository, pessoaRepository, vinculoRepository);
	}

	@Test
	public void shouldCriarOrganograma() {
		CriarOrganogramaDTO criarOrganogramaDTO = new CriarOrganogramaDTO();
		Organograma organogramaSalvo = new Organograma();
		when(organogramaRepository.save(any(Organograma.class))).thenReturn(organogramaSalvo);

		OrganogramaDTO actualValue = organogramaServiceImpl.criarOrganograma(criarOrganogramaDTO);

		assertThat(actualValue, notNullValue());
	}

	@Test
	public void shouldAtualizarOrganograma() {
		Long organogramaId = 1L;
		CriarOrganogramaDTO criarOrganogramaDTO = new CriarOrganogramaDTO();
		Organograma organogramaExistente = new Organograma();
		when(organogramaRepository.findById(organogramaId)).thenReturn(Optional.of(organogramaExistente));

		OrganogramaDTO actualValue = organogramaServiceImpl.atualizarOrganograma(organogramaId, criarOrganogramaDTO);

		assertThat(actualValue, notNullValue());
	}

	@Test
	public void shouldExcluirOrganograma() {
		Long organogramaId = 1L;

		organogramaServiceImpl.excluirOrganograma(organogramaId);

		verify(organogramaRepository, times(1)).deleteById(organogramaId);
	}

	@Test
	public void shouldConsultarUnidadesPorNome() {
		String nome = "Unidade A";

		List<Unidade> unidades = new ArrayList<>();
		Unidade unidade1 = new Unidade();
		unidade1.setId(1L);
		unidade1.setNome("Unidade A");
		unidades.add(unidade1);

		when(unidadeRepository.findByNomeIgnoreCaseContaining(nome)).thenReturn(unidades);

		List<ConsultarUnidadeDTO> actualValue = organogramaServiceImpl.consultarUnidadesPorNome(nome);

		assertThat(actualValue, notNullValue());
		assertEquals(1, actualValue.size());

		ConsultarUnidadeDTO unidadeDto = actualValue.get(0);
		assertThat(unidadeDto.getId(), equalTo(1L));
		assertThat(unidadeDto.getNome(), equalTo("Unidade A"));
	}

	@Test
	public void shouldConsultarPessoasPorNome() {
		String nome = "John Doe";

		List<Pessoa> pessoas = new ArrayList<>();
		Pessoa pessoa1 = new Pessoa();
		pessoa1.setId(1L);
		pessoa1.setNome("John Doe");
		pessoas.add(pessoa1);

		when(pessoaRepository.findByNomeIgnoreCaseContaining(nome)).thenReturn(pessoas);

		List<ConsultarPessoaPorNomeDTO> actualValue = organogramaServiceImpl.consultarPessoasPorNome(nome);

		assertThat(actualValue, notNullValue());
		assertEquals(1, actualValue.size());

		ConsultarPessoaPorNomeDTO pessoaDto = actualValue.get(0);
		assertThat(pessoaDto.getId(), equalTo(1L));
		assertThat(pessoaDto.getNome(), equalTo("John Doe"));
	}

	@Test
	public void shouldCriarPessoa() {
		CriarPessoaDTO criarPessoaDTO = new CriarPessoaDTO();
		Pessoa pessoaSalva = new Pessoa();
		when(pessoaRepository.save(any(Pessoa.class))).thenReturn(pessoaSalva);

		ConsultarPessoaPorNomeDTO actualValue = organogramaServiceImpl.criarPessoa(criarPessoaDTO);

		assertThat(actualValue, notNullValue());
	}

	@Test
	public void shouldAtualizarPessoa() {
		Long pessoaId = 1L;
		AtualizarPessoaDTO atualizarPessoaDTO = new AtualizarPessoaDTO();
		Pessoa pessoaExistente = new Pessoa();
		when(pessoaRepository.findById(pessoaId)).thenReturn(Optional.of(pessoaExistente));

		ConsultarPessoaPorNomeDTO actualValue = organogramaServiceImpl.atualizarPessoa(pessoaId, atualizarPessoaDTO);

		assertThat(actualValue, notNullValue());
	}

	@Test
	public void shouldExcluirPessoa() {
		Long pessoaId = 1L;

		organogramaServiceImpl.excluirPessoa(pessoaId);

		verify(pessoaRepository, times(1)).deleteById(pessoaId);
	}
}
